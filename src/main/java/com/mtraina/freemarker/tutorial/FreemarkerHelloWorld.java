package com.mtraina.freemarker.tutorial;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Matteo
 * Date: 03/10/2013
 * Time: 19:30
 * To change this template use File | Settings | File Templates.
 */
public class FreemarkerHelloWorld {
    public static void main(String[] args){
        Configuration cfg = new Configuration();
        try{
            Template template = cfg.getTemplate("src/main/resources/helloworld.ftl");

            Map<String, Object> data = new HashMap<String, Object>(2);
            data.put("message", "Hello world!");

            List<String> countries = new ArrayList<String>(3);
            countries.add("Italy");
            countries.add("UK");
            countries.add("Spain");
            data.put("countries", countries);

            Writer out = new OutputStreamWriter(System.out);
            template.process(data, out);
            out.flush();

            // File output
            Writer file = new FileWriter (new File("src/main/resources/helloworld.txt"));
            template.process(data, file);
            file.flush();
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
